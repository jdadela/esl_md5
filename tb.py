from myhdl import block, instance, Signal, ResetSignal, StopSimulation, instances, delay, intbv
from md5_myhdl import md5
import os

from myhdl_sim import clk_stim
from axi import Axis

@block
def testbench(vhdl_output_path=None):
    reset = ResetSignal(0, active=0, async=False)
    clk = Signal(bool(0))

    axis_raw = Axis(32)
    axis_sum = Axis(32)
    clk_gen = clk_stim(clk, period=10)

    @instance
    def reset_gen():
        reset.next = 0
        yield delay(10)
        yield clk.negedge
        reset.next = 1

    @instance
    def write_stim():
        dana1 = b"AAAA"
        # dana2 = 0x45464748
        values = bytearray(dana1)
        i = 0
        yield reset.posedge
        iter_num = 1
        while i < iter_num * len(values):
            yield clk.negedge
            axis_raw.tvalid.next = 1
            axis_raw.tdata.next = int.from_bytes(values[i % len(values) : i % len(values)+4], byteorder='little')
            if i == iter_num * len(values) - 4:
                axis_raw.tlast.next = 1
            else:
                axis_raw.tlast.next = 0
            if axis_raw.tready == 1:
                i += 4
        yield clk.negedge
        axis_raw.tvalid.next = 0

    @instance
    def read_stim():
        yield reset.posedge
        yield delay(100)
        yield clk.negedge
        axis_sum.tready.next = 1
        while True:
            yield clk.negedge
            if axis_sum.tlast == 1:
                break

        for i in range(200):
            yield clk.negedge
        raise StopSimulation()

    uut = md5(clk, reset, axis_raw, axis_sum)

    if vhdl_output_path is not None:
        uut.convert(hdl='VHDL', path=vhdl_output_path)
    return instances()


if __name__ == '__main__':
    trace_save_path = '../out/testbench/'
    vhdl_output_path = '../out/vhdl/'
    os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
    os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)

    tb_ = testbench(vhdl_output_path)
    # tb_ = testbench()
    tb_.config_sim(trace=True, directory=trace_save_path, name='tb')
    tb_.run_sim()
