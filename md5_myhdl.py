from myhdl import block, always_seq, Signal, intbv, enum, instances, always_comb
import math

# -------------------------------------------------
#   constants
# -------------------------------------------------
k = [int(abs(math.sin(i + 1)) * 2 ** 32) & 0xFFFFFFFF for i in range(64)]
rotate_amounts = [7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22, 7, 12, 17, 22,
                  5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20, 5, 9, 14, 20,
                  4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23, 4, 11, 16, 23,
                  6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21, 6, 10, 15, 21]
a_const = 0x67452301
b_const = 0xefcdab89
c_const = 0x98badcfe
d_const = 0x10325476


# -------------------------------------------------
#   functions
# -------------------------------------------------
def left_rotate(x, amount):
    x &= 0xFFFFFFFF
    return ((x << amount) | (x >> (32 - amount))) & 0xFFFFFFFF


# -------------------------------------------------
#   myhdl
# -------------------------------------------------
@block
def md5(clk, reset, axis_s_raw, axis_m_sum):
    buff1ready_set = Signal(bool(0))
    buff1ready_clr = Signal(bool(0))
    buff1ready_out = Signal(bool(0))
    datacount = Signal(intbv(0, min=0, max=2 ** 32 - 1))  # defines BYTE length
    databuff = [Signal(intbv(0, min=0, max=2 ** 8 - 1)) for i in range(64)]
    inputend = Signal(bool(0))
    appended = Signal(bool(0))
    datatosend = [Signal(intbv(0, min=0, max=2 ** 32 - 1)) for i in range(4)]
    readytosend_set = Signal(bool(0))
    readytosend_clr = Signal(bool(0))
    readytosend_out = Signal(bool(0))
    sendingindex = Signal(intbv(0, min=0, max=127))
    bit_size = Signal(intbv(0, min=0, max=2 ** 64 - 1))

    rotate_amounts_sig = [Signal(intbv(val=rotate_amounts[i], min=0, max=30)) for i in range(len(rotate_amounts))]
    k_sig = [Signal(intbv(val=k[i], min=0, max=2**32)) for i in range(len(k))]

    # -------------------------------------------------
    #   readytosend - combinational
    # -------------------------------------------------
    @always_comb
    def comb_signal_drive():
        readytosend_out.next = readytosend_clr ^ readytosend_set
        buff1ready_out.next = buff1ready_clr ^ buff1ready_set
        axis_s_raw.tready.next = not (buff1ready_clr ^ buff1ready_set)  # = not buff1ready_out

    # -------------------------------------------------
    #   data input, appending with 0 and size
    # -------------------------------------------------
    @always_seq(clk.posedge, reset=reset)
    def data_in():
        if (axis_s_raw.tvalid == 1 and axis_s_raw.tlast == 0 and inputend == 0) or (axis_s_raw.tlast == 1 and axis_s_raw.tvalid == 1):  # incoming data word
            # buffer ready to accept new data
            if buff1ready_out == 0:
                databuff[datacount % 64 + 0].next = axis_s_raw.tdata[8:0]
                databuff[datacount % 64 + 1].next = axis_s_raw.tdata[16:8]
                databuff[datacount % 64 + 2].next = axis_s_raw.tdata[24:16]
                databuff[datacount % 64 + 3].next = axis_s_raw.tdata[32:24]
                datacount.next = datacount + 4
                if (datacount + 4) % 64 == 0:
                    buff1ready_set.next = not buff1ready_set

        if axis_s_raw.tlast == 1 and axis_s_raw.tvalid == 0 and inputend == 0 and buff1ready_out == 0:   # end of data
            if bit_size == 0:
                bit_size.next = 8 * datacount
                databuff[datacount % 64].next = 128
            # append with 0 until desired size oz half buffer full
            if (datacount + 1) % 64 != 56 and appended == 0:
                databuff[(datacount + 1) % 64].next = 0
                datacount.next = datacount + 1
            else:
                appended.next = True

            if appended == 1 and inputend == 0:  # append with size
                databuff[datacount % 64 + 1].next = bit_size[8:0]
                databuff[datacount % 64 + 2].next = bit_size[16:8]
                databuff[datacount % 64 + 3].next = bit_size[24:16]
                databuff[datacount % 64 + 4].next = bit_size[32:24]
                databuff[datacount % 64 + 5].next = bit_size[40:32]
                databuff[datacount % 64 + 6].next = bit_size[48:40]
                databuff[datacount % 64 + 7].next = bit_size[56:48]
                databuff[datacount % 64 + 8].next = bit_size[64:56]
                datacount.next = datacount + 9
                if (datacount + 9) % 64 == 0 and datacount > 0:  # second half ready
                    buff1ready_set.next = not buff1ready_set
                inputend.next = True

    # -------------------------------------------------
    #   md5 calculation
    # -------------------------------------------------
    # signals used during calculation
    a0 = Signal(intbv()[32:])
    b0 = Signal(intbv()[32:])
    c0 = Signal(intbv()[32:])
    d0 = Signal(intbv()[32:])
    # b0 = Signal(intbv(val=0xefcdab89, _nrbits=32))
    # c0 = Signal(intbv(val=0x98badcfe, _nrbits=32))
    # d0 = Signal(intbv(val=0x10325476, _nrbits=32))
    # startindex = Signal(intbv(1, min=0, max=65))  # 0/64 is 1st/2nd buff, 1 means uninitialized
    # prevstartindex = Signal(intbv(1, min=0, max=65))  # 0/64 is 1st/2nd buff, 1 means uninitialized
    i = Signal(intbv(val=0, min=0, max=66))  # md5 loop iterator (1-64)

    a = Signal(intbv(val=0, min=0, max=2 ** 32 - 1))  # variables used during calculation
    b = Signal(intbv(val=0, min=0, max=2 ** 32 - 1))
    c = Signal(intbv(val=0, min=0, max=2 ** 32 - 1))
    d = Signal(intbv(val=0, min=0, max=2 ** 32 - 1))

    @always_seq(clk.posedge, reset=reset)
    def calc_md5():
        if datacount == 0:  # initialize values (necessary for multiple md5 calculations)
            a0.next = intbv(a_const)[32:]
            b0.next = intbv(b_const)[32:]
            c0.next = intbv(c_const)[32:]
            d0.next = intbv(d_const)[32:]

        # init a, b, c, d, i
        if buff1ready_out == 1 and i == 0:
            a.next = a0
            b.next = b0
            c.next = c0
            d.next = d0
            i.next = 1

        if i > 0:
            g = intbv(0, _nrbits=32)
            f = intbv(0, _nrbits=64)
            if i <= 16:
                f[:] = ((b & c) | (~b & d))
                g[:] = i - 1
            elif i <= 32:
                f[:] = (d & b) | (~d & c)
                g[:] = (5 * (i - 1) + 1) % 16
            elif i <= 48:
                f[:] = b ^ c ^ d
                g[:] = (3 * (i - 1) + 5) % 16
            elif i <= 64:
                f[:] = c ^ (b | ~d)
                g[:] = (7 * (i - 1)) % 16

            if i <= 64:
                x = intbv(0, _nrbits=32)
                x[:] = databuff[4 * g] | databuff[4 * g + 1] << 8 \
                       | databuff[4 * g + 2] << 16 | databuff[4 * g + 3] << 24
                # f1 = intbv(0, _nrbits=64)
                f[:] = (f + a + k_sig[i - 1] + x)
                # f[:] = f1 & 0xFFFFFFFF
                a.next = d
                d.next = c
                c.next = b
                # b.next = (b + left_rotate(f, rotate_amounts[i - 1])) & 0xFFFFFFFF

                # b.next = (b + (((f << rotate_amounts_sig[i - 1]) | (f >> (32 - rotate_amounts_sig[i - 1]))) & 0xFFFFFFFF)) & 0xFFFFFFFF
                b.next = (b + ((((f & 0xFFFFFFFF) << rotate_amounts_sig[i-1]) | ((f & 0xFFFFFFFF) >> (32 - rotate_amounts_sig[i-1]))) & 0xFFFFFFFF)) & 0xFFFFFFFF
                # ((((f & 0xFFFFFFFF) << rotate_amounts_sig[i-1]) | ((f & 0xFFFFFFFF) >> (32 - rotate_amounts_sig[i-1]))) & 0xFFFFFFFF)

                i.next = i + 1

            if i == 65:  # 1 cycle to get latest values into a, b, c, d
                i.next = 0
                a0.next = (a0 + a)# & 0xFFFFFFFF
                b0.next = (b0 + b) & 0xFFFFFFFF
                c0.next = (c0 + c) & 0xFFFFFFFF
                d0.next = (d0 + d) & 0xFFFFFFFF
                buff1ready_clr.next = not buff1ready_clr

        if buff1ready_out == 0 and inputend == 1 and readytosend_out == 0:
            datatosend[0].next = a0
            datatosend[1].next = b0
            datatosend[2].next = c0
            datatosend[3].next = d0
            if readytosend_clr == 0:
                readytosend_set.next = not readytosend_set

    # -------------------------------------------------
    #   sending result
    # -------------------------------------------------

    @always_seq(clk.posedge, reset=reset)
    def send_data():
        if readytosend_out == 1:
            axis_m_sum.tdata.next = datatosend[sendingindex]
            axis_m_sum.tvalid.next = True
            if sendingindex < 3:
                axis_m_sum.tlast.next = False
            else:
                axis_m_sum.tlast.next = True
            if axis_m_sum.tready == 1:
                sendingindex.next = sendingindex + 1
                if sendingindex == 3:
                    sendingindex.next = 0
                    readytosend_clr.next = not readytosend_clr
                    # axis_m_sum.tvalid.next = 0
                    # datacount.next = 0
                    # inputend.next = 0
                    # appended.next = 0

    return instances()
